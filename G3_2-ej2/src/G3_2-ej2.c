/*
 ============================================================================
 Name        : G3_2-ej2.c
 Author      : MGP
 Version     :
 Copyright   : Free code babe
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {      //Los argumentos "entre comillas" se toman como uno sólo

    /* Variables */
    FILE *archivo_txt;
    char argf[255], w[255]; //argf contendrá el archivo del usuario
    unsigned palabras = 0;

    /* Inicio */
    printf(" *** Bienvenido al Cuentapalabras ***\n\n");

    //1. Verifico invocacion sea con 1 argumento
    --argc; //adapto este valor porque la llamada al programa se considera un argumento
    if (1 != argc) {
        printf("Debes invocarme con el nombre de 1 archivo como argumento\nTERMINADO!\n\n");
        return EXIT_FAILURE;
    }

    //2. Sabemos que hay argumento, veo si es correcto
    strcpy(argf, argv[1]);
    archivo_txt = fopen(argf, "r");
    if (!archivo_txt) {
        printf("\nNo se hallo un archivo '%s'.\nTERMINADO!\n\n", argf);
        return EXIT_FAILURE;
    }

    //3. Ya sabemos que el archivo existe, a contar palabras...
    archivo_txt = fopen(argf, "r");
    fseek(archivo_txt, 0L, SEEK_SET);    //SEEK_SET lleva el puntero al inicio
    while(1 == fscanf(archivo_txt, " %255s", w)) {
        ++palabras;
    }
    fclose(archivo_txt);

    //4. Conteo terminado, se presenta el resultado
    printf("El archivo '%s' contiene %i palabras.\n\n", argf, palabras);
    return EXIT_SUCCESS;
}


