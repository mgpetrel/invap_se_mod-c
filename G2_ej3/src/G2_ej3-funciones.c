#include "G2_ej3.h"

/*  recibe: datos de 1 jugador y del mazo de cartas
 *
 *  1. Toma una carta del mazo al azar y la entrega a 1 jugador
 *  2. Toma nota que esa carta se retiró del mazo
 *  3. Imprime los detalles de la carta entregada: palo, ordinal, valor-puntaje
 *  4. Actualiza el puntaje acumulado del jugador teniendo en cuenta el reglamento
 *
 *  devuelve: puntaje que lleva acumulado el jugador
 */
uint8_t repartir_carta(jregistro_t* jugador, mazo_frances_t* mazo){

    uint8_t palo, cart, carta_tomada = FALSE;

    /* determina palo y carta al azar */
    while(!carta_tomada) {
        palo = rand()%CPALOS;
        cart = rand()%CARTxP;
        if (mazo->carta[palo][cart]) {
            mazo->carta[palo][cart] = FALSE;
            carta_tomada = TRUE;
            --(mazo->cartas_disponibles);
        }
    }
    palo += 1;  // (0-> 3) transformo a (1-> 4) por motivos esteticos
    cart += 1;  // (0->12) transformo a (1->13) por motivos de valor-puntaje

    printf("tu carta es:\t");
    printf("Palo= %i\t", palo);
    printf("Ordinal= %i\t", cart);

    /* ajusta el puntaje de la carta si corresponde*/
    if (cart > FIGURA) cart = FIGURA;
    else if(AS == cart) {
        cart = AS_SUPER;
        jugador->tiene_as = TRUE;
    }
    printf("Valor: %i\t", cart);

    jugador->puntos_acumulados += cart;
    if((jugador->tiene_as) && (jugador->puntos_acumulados > BLACKJACK)) {
        jugador->puntos_acumulados = jugador->puntos_acumulados - 10;
        jugador->tiene_as = FALSE;  //evitamos que el mismo AS repita su 'rebaja'
    }
    return jugador->puntos_acumulados;
}

/*  recibe: puntaje final de ambos jugadores
 *
 *  1. Compara los puntajes y determina un ganador o empate
 *
 *  devuelve: un código que representa el resultado
 */
uint8_t analizar_puntajes_y_determinar_ganador(uint8_t* puntos){

    uint8_t winner;

    if     (puntos[J1] > 21)               winner = J2;
    else if(puntos[J2] > 21)               winner = J1;
    else if(puntos[J1] == puntos[J2])      winner = EMPATE;
    else {  //ambos jugadores tienen menos que 21 puntos y puntaje diferente
        puntos[J1] = 21 - puntos[J1];
        puntos[J2] = 21 - puntos[J2];
        winner = (puntos[J2] > puntos[J1]) ? J1 : J2;
    }
    return winner;

}
