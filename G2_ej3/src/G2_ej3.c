/*
 ============================================================================
 Name        : G2_ej3.c
 Author      : MGP
 Version     :
 Copyright   : Free code babe
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "G2_ej3.h"

int main(void) {

    /* Variables */
    mazo_frances_t mazo;              //cartas
    jregistro_t jugador[JCANT];       //jugadores
    uint8_t i, j, puntos[JCANT], TheWinnerIs;
    char quiere_carta;

    /* Inicialización */
    srand((unsigned)time(NULL));      //semilla random

    for(i=0; i<CPALOS; ++i)           // todos los palos
        for (j=0; j<CARTxP; ++j)      // todos los numeros
            mazo.carta[i][j] = TRUE;  // TRUE = carta en el mazo
    mazo.cartas_disponibles = CPALOS*CARTxP;

    for (i=0; i<JCANT; ++i) {       // jugadores inician sin puntos
        jugador[i].puntos_acumulados = 0;
        jugador[i].tiene_as = FALSE;
        jugador[i].quiere_otra_carta = TRUE;
    }

    /* Juego - inicio */
    printf("***Bienvenidos al Black Jack***\n\n");

    //1. REPARTIR CARTAS HASTA STOP VOLUNTARIO O PASAR DE 21 PUNTOS
    uint8_t manos = 0, alguno_quiere_mas_cartas = TRUE;

    while(alguno_quiere_mas_cartas) {

        printf("Jugador #1, ");
        if((manos >= 2) && (jugador[J1].quiere_otra_carta)) {
            //Luego de darle la 2da carta preguntamos al jugador 1 si quiere otra
            printf("quieres otra carta? ('n' para NO / 's' para SI) : "); scanf(" %c", &quiere_carta);
            if('n' == quiere_carta) jugador[J1].quiere_otra_carta = FALSE;
        }
        if(jugador[J1].quiere_otra_carta) {
            if((puntos[J1] = repartir_carta(&jugador[J1], &mazo)) > BLACKJACK) {
                printf("te pasaste de puntos! (%i puntos)\n", jugador[J1].puntos_acumulados);
                break;
            }
        }
        printf("puntaje hasta aquí: %i\n", jugador[J1].puntos_acumulados);

        printf("Jugador #2, ");
        if((manos >= 2) && (jugador[J2].quiere_otra_carta)) {
            //Luego de darle la 2da carta preguntamos al jugador 2 si quiere otra
            printf("quieres otra carta? ('n' para NO / 's' para SI) : "); scanf(" %c", &quiere_carta);
            if('n' == quiere_carta) jugador[J2].quiere_otra_carta = FALSE;
        }
        if(jugador[J2].quiere_otra_carta) {
            if((puntos[J2] = repartir_carta(&jugador[J2], &mazo)) > BLACKJACK) {
                printf("te pasaste de puntos! (%i puntos)\n", jugador[J2].puntos_acumulados);
                break;
            }
        }
        printf("puntaje hasta aquí: %i\n", jugador[J2].puntos_acumulados);

        printf("\n");
        //ya repartimos una mano
        ++manos;

        alguno_quiere_mas_cartas = (jugador[J1].quiere_otra_carta | jugador[J2].quiere_otra_carta);
    }

    //ANALIZAR PUNTAJES Y DETERMINAR GANADOR
    TheWinnerIs = analizar_puntajes_y_determinar_ganador(puntos);
    printf("\n\nAnd the winner issssss.... \n\n");
    switch(TheWinnerIs){
        case J1:
            printf("Jugador #1 !!\n\n");
            break;
        case J2:
            printf("Jugador #2 !!\n\n");
            break;
        default:
            printf("EMPATE! (nadie sale lastimado)\n\n");
            break;
    }

    return EXIT_SUCCESS;
}
