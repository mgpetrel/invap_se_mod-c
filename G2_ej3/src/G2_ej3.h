/*
 * G2_ej3.h
 *
 *  Created on: Aug 19, 2021
 *      Author: developer
 */

#ifndef G2_EJ3_H_
#define G2_EJ3_H_

/* Includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

/* Defines */
#define JCANT      2   //Cantidad de jugadores
#define CPALOS     4   //Cantidad de palos
#define CARTxP    13   //Cantidad de cartas por palo
#define AS         1
#define FIGURA    10
#define AS_SUPER  11
#define BLACKJACK 21

/* Enums */
enum { FALSE, TRUE };
enum { J1, J2, EMPATE };

/* Typedefs */
typedef struct {    //Registro de jugador
    uint8_t puntos_acumulados;
    uint8_t tiene_as;
    uint8_t quiere_otra_carta;
} jregistro_t;
typedef struct {    //Mazo de cartas
    uint8_t carta[CPALOS][CARTxP];
    uint8_t cartas_disponibles;
} mazo_frances_t;

/* Functions */
uint8_t repartir_carta(jregistro_t* jugador, mazo_frances_t* mazo);
uint8_t analizar_puntajes_y_determinar_ganador(uint8_t* puntos);

#endif /* G2_EJ3_H_ */
